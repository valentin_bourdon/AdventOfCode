﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Collections;

namespace AdventOfCode2021
{
    public class Day_11 : BaseDay
    {
        string[] inputs;
        private Dictionary<int, List<int>> energyLevels;
        private int totalOfFlashes = 0;
        int numberOfFlashes = 0;

        public Day_11()
        {
            energyLevels = new Dictionary<int, List<int>>();
            ReadInput();
        }

        public override ValueTask<string> Solve_1()
        {
            for (int i = 0; i < 100; i++)
                totalOfFlashes += SolveEnergyLevel(energyLevels);

            return new(totalOfFlashes.ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            energyLevels = new Dictionary<int, List<int>>();
            ReadInput();

            int count = 0;
            int step = 0;
            while (count != 100)//Determine the step when all octopus are flashes (100 => 10*10 grid)
            {
                count = SolveEnergyLevel(energyLevels);
                step++;
            }

            return new(step.ToString());
        }


       
        public int SolveEnergyLevel(Dictionary<int, List<int>> energyLevels)
        {
            numberOfFlashes = 0;

            // Directions
            int[,] dir = { { 0, 1 }, { 0, -1 }, // Right - left
                            { 1, 0 }, { -1, 0 }, // Down - Up
                            { 1, -1 }, { -1, -1},//Diagonal up right - Diaginal up left
                            { 1, 1 }, { -1, 1}//Diagonal down right - Diaginal down left
            };

            // Queue
            Queue q = new Queue();

            // Insert the first point coordinate
            for (int i = 0; i < energyLevels.Count; i++)
            {
                for (int j = 0; j < energyLevels[i].Count; j++)
                {
                    energyLevels[i][j] += 1;
                    Tuple<int, int> pos = new Tuple<int, int>(i, j);
                    q.Enqueue(pos);
                }
            }

            // Until queue is empty
            while (q.Count > 0)
            {
                Tuple<int, int> p = (Tuple<int, int>)(q.Peek());
                q.Dequeue();

                // Mark as visited
                if(energyLevels[p.Item1][p.Item2] > 9)
                {
                    energyLevels[p.Item1][p.Item2] = 0;
                    numberOfFlashes += 1;
                    q.Enqueue(new Tuple<int, int>(p.Item1, p.Item2));
                }
                else if(energyLevels[p.Item1][p.Item2] == 0)
                {
                    // Check all four directions
                    for (int i = 0; i < 8; i++)
                    {
                        // Using the direction array
                        int a = p.Item1 + (dir[i, 0]);
                        int b = p.Item2 + (dir[i, 1]);

                        // Not blocked and valid
                        if (a >= 0 && b >= 0 && a < energyLevels.Count && b < energyLevels[a].Count)
                        {
                            if (energyLevels[a][b] != 0)
                            {
                                energyLevels[a][b] += 1;
                                if (energyLevels[a][b] > 9)
                                {
                                    energyLevels[a][b] = 0;
                                    numberOfFlashes += 1;
                                    q.Enqueue(new Tuple<int, int>(a, b));
                                }
                            }
                        }
                    }
                }
            }

            return numberOfFlashes;
        }
        

        private void ReadInput()
        {
            inputs = File.ReadAllLines(InputFilePath);

            for (int i = 0; i < inputs.Length; i++)
            {
                List<int> values = inputs[i].Select(x => int.Parse(x.ToString())).ToList();
                energyLevels.Add(i, values);
            }
        }
    }
}

﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace AdventOfCode2021
{
    public class Day_15 : BaseDay
    {
        string[] inputs;

        private Dictionary<int, List<int>> map;

        private Astar A;

        public Day_15()
        {
            map = new Dictionary<int, List<int>>();

            ReadInput();
            CreateGrid();

           
            
        }

        public override ValueTask<string> Solve_1()
        {
            Node start = new Node(0, 0, map[0][0]);
            Node end = new Node(map.Count - 1, map[map.Count - 1].Count - 1, map[map.Count - 1][map.Count - 1]);

            Stack<Node> path = A.FindPath(start, end);

            return new(path.Select(node => node.Weight).Sum().ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            throw new NotImplementedException();
        }

        private void ReadInput()
        {
            inputs = File.ReadAllLines(InputFilePath);
        }

        void CreateGrid()
        {
            for (int i = 0; i < inputs.Length; i++)
            {
                List<int> values = inputs[i].Select(x => int.Parse(x.ToString())).ToList();
                map.Add(i, values);
            }

            List<List<Node>> temp = new List<List<Node>>();
            for (int row = 0; row < map.Count; row++)
            {
                temp.Add(new List<Node>());
                for (int column = 0; column < map[row].Count; column++)
                {
                    temp[row].Add(new Node(row, column, map[row][column]));
                }
            }

            A = new Astar(temp);
        }

        public class Node
        {
            public int X;
            public int Y;
            public float Cost;
            public float Distance;
            public float Weight;
            public float CostDistance => Cost + Distance;
            public Node Parent;

            public Node(int _x, int _y, int _weight)
            {
                Parent = null;
                X = _x;
                Y = _y;
                Distance = -1;
                Cost = 1;
                Weight = _weight;
            }

            //The distance is essentially the estimated distance, ignoring walls to our target. 
            //So how many tiles left and right, up and down, ignoring walls, to get there. 
            public void SetDistance(int targetX, int targetY)
            {
                this.Distance = Math.Abs(targetX - X) + Math.Abs(targetY - Y);
            }
        }

        public class Astar
        {
            List<List<Node>> Grid;

            //Get the number of rows in the grid
            int GridRows
            {
                get
                {
                    return Grid[0].Count;
                }
            }

            //Get the number of columns in the grid
            int GridCols
            {
                get
                {
                    return Grid.Count;
                }
            }

            public Astar(List<List<Node>> grid)
            {
                Grid = grid;
            }

            public Stack<Node> FindPath(Node Start, Node End)
            {
                //Node start = new Node(new Vector2((int)(Start.X / Node.NODE_SIZE), (int)(Start.Y / Node.NODE_SIZE)), true);
                //Node end = new Node(new Vector2((int)(End.X / Node.NODE_SIZE), (int)(End.Y / Node.NODE_SIZE)), true);

                Stack<Node> Path = new Stack<Node>();
                List<Node> OpenList = new List<Node>();
                List<Node> ClosedList = new List<Node>();
                List<Node> adjacencies;
                Node current = Start;

                // add start node to Open List
                OpenList.Add(Start);

                while (OpenList.Count != 0 && !ClosedList.Exists(x => x.X == End.X && x.Y == End.Y))
                {
                    current = OpenList[0];
                    OpenList.Remove(current);
                    ClosedList.Add(current);
                    adjacencies = GetNeighborsNodes(current);

                    foreach (Node n in adjacencies)
                    {
                        if (!ClosedList.Contains(n))
                        {
                            if (!OpenList.Contains(n))
                            {
                                n.Parent = current;
                                n.Distance = Math.Abs(n.X - End.X) + Math.Abs(n.Y - End.Y);
                                n.Cost = n.Weight + n.Parent.Cost;
                                OpenList.Add(n);
                                OpenList = OpenList.OrderBy(node => node.CostDistance).ToList<Node>();
                            }
                        }
                    }
                }


                // construct path, if end was not closed return null
                if (!ClosedList.Exists(x => x.X == End.X && x.Y == End.Y))
                {
                    return null;
                }

                // if all good, return path
                Node temp = ClosedList[ClosedList.IndexOf(current)];
                if (temp == null) return null;
                do
                {
                    Path.Push(temp);
                    temp = temp.Parent;
                } while (temp != Start && temp != null);

                return Path;
            }

            /// <summary>
            /// Return all neighbors of a node
            /// </summary>
            /// <param name="n"></param>
            /// <returns></returns>
            private List<Node> GetNeighborsNodes(Node n)
            {
                List<Node> temp = new List<Node>();

                int row = (int)n.Y;
                int col = (int)n.X;

                

                if (row + 1 < GridRows)
                {
                    temp.Add(Grid[col][row + 1]);
                }
                if (row - 1 >= 0)
                {
                    temp.Add(Grid[col][row - 1]);
                }
                if (col - 1 >= 0)
                {
                    temp.Add(Grid[col - 1][row]);
                }
                if (col + 1 < GridCols)
                {
                    temp.Add(Grid[col + 1][row]);
                }

                return temp;
            }
        }
    }
}

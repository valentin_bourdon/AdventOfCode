﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;

namespace AdventOfCode2021
{
    public class Day_03 : BaseDay
    {
        private string gammeaRate;
        private string epsilonRate;
        private int powerConsumption;
        private string oxygenGeneratorRating;
        private string CO2ScrubberRating;
        private int lifeSupportRating;

        private List<char[]> oxygenGeneratorInputs;
        private List<char[]> CO2ScrubberInputs;

        private string[] input;
        private List<char[]> binaryNumbers;
        private int lengthBinaryNumbers;

        private int numberOfByteZero;
        private int numberOfByteOne;

        public Day_03()
        {
            binaryNumbers = new List<char[]>();

            ReadInput();
        }

        /// <summary>
        /// Use diagnostic report to generate two new binary numbers (called the gamma rate and the epsilon rate) and determine the power consumption.
        /// </summary>
        /// <returns></returns>
        public override ValueTask<string> Solve_1()
        {
            for (int i = 0; i < lengthBinaryNumbers; i++)
            {
                numberOfByteZero = 0;
                numberOfByteOne = 0;

                for (int j = 0; j < binaryNumbers.Count; j++)
                {
                    if(binaryNumbers[j][i] == '1')
                        numberOfByteOne += 1;
                    else if (binaryNumbers[j][i] == '0')
                        numberOfByteZero += 1;
                }

                if (numberOfByteOne > numberOfByteZero)
                {
                    gammeaRate += "1";
                    epsilonRate += "0";
                }
                else if (numberOfByteOne < numberOfByteZero)
                {
                    gammeaRate += "0";
                    epsilonRate += "1";
                }
            }

            powerConsumption = Convert.ToInt32(gammeaRate, 2) * Convert.ToInt32(epsilonRate, 2);

            return new(powerConsumption.ToString());
        }

        /// <summary>
        /// Verify the life support rating, which can be determined by multiplying the oxygen generator rating by the CO2 scrubber rating.
        /// </summary>
        /// <returns></returns>
        public override ValueTask<string> Solve_2()
        {
            CO2ScrubberInputs = new List<char[]>(binaryNumbers);
            CO2ScrubberRating = new string(GetCO2ScrubblerRating(CO2ScrubberInputs));

            oxygenGeneratorInputs = new List<char[]>(binaryNumbers);
            oxygenGeneratorRating = new string(GetOxygenRating(oxygenGeneratorInputs));

            lifeSupportRating = Convert.ToInt32(oxygenGeneratorRating, 2) * Convert.ToInt32(CO2ScrubberRating, 2);

            return new(lifeSupportRating.ToString());
        }

        /// <summary>
        /// Find oxygen generator rating
        /// </summary>
        /// <param name="_inputs">Diagnostic report values</param>
        /// <returns></returns>
        public char[] GetOxygenRating(List<char[]> _inputs)
        {
            for (int i = 0; i < lengthBinaryNumbers; i++)
            {
                numberOfByteZero = 0;
                numberOfByteOne = 0;

                //Get number of byte '1' and byte '0' 
                for (int j = 0; j < _inputs.Count; j++)
                {
                    if (_inputs[j][i] == '1')
                        numberOfByteOne += 1;
                    else if (_inputs[j][i] == '0')
                        numberOfByteZero += 1;
                }


                if (numberOfByteOne > numberOfByteZero)//Keep only binary numbers starting by '1'
                {
                    for (int w = _inputs.Count - 1; w >= 0; w--)
                        if (_inputs[w][i] != '1')
                            _inputs.RemoveAt(w);
                }
                else if (numberOfByteZero > numberOfByteOne)// Keep only binary numbers starting by '0'
                {
                    for (int w = _inputs.Count - 1; w >= 0; w--)
                        if (_inputs[w][i] != '0')
                            _inputs.RemoveAt(w);
                }
                else if (numberOfByteOne == numberOfByteZero)// Keep only binary numbers starting by '1'
                {
                    for (int w = _inputs.Count - 1; w >= 0; w--)
                        if (_inputs[w][i] != '1')
                            _inputs.RemoveAt(w);
                }

                
                if (_inputs.Count == 1)//Return final binary number
                    return _inputs[0];
            }

            return null;
        }

        /// <summary>
        /// Find CO2 scrubber rating
        /// </summary>
        /// <param name="_inputs">Diagnostic report values</param>
        /// <returns></returns>
        public char[] GetCO2ScrubblerRating(List<char[]> _inputs)
        {
            for (int i = 0; i < lengthBinaryNumbers; i++)
            {
                numberOfByteZero = 0;
                numberOfByteOne = 0;

                for (int j = 0; j < _inputs.Count; j++)
                {
                    if (_inputs[j][i] == '1')
                        numberOfByteOne += 1;
                    else if (_inputs[j][i] == '0')
                        numberOfByteZero += 1;
                }


                if (numberOfByteOne > numberOfByteZero)//Keep only binary numbers starting by '0'
                {
                    for (int w = _inputs.Count - 1; w >= 0; w--)
                        if (_inputs[w][i] != '0')
                            _inputs.RemoveAt(w);
                }
                else if (numberOfByteZero > numberOfByteOne)//Keep only binary numbers starting by '1'
                {
                    for (int w = _inputs.Count - 1; w >= 0; w--)
                        if (_inputs[w][i] != '1')
                            _inputs.RemoveAt(w);
                }
                else if (numberOfByteOne == numberOfByteZero)//Keep only binary numbers starting by '0'
                {
                    for (int w = _inputs.Count - 1; w >= 0; w--)
                        if (_inputs[w][i] != '0')
                            _inputs.RemoveAt(w);
                }

                if (_inputs.Count == 1)//Return final binary number
                    return _inputs[0];
            }

            return null;
        }

        private void ReadInput()
        {
            input = File.ReadAllLines(InputFilePath);

            lengthBinaryNumbers = input[0].Length;

            for (int i = 0; i < input.Length; i++)
            {
                binaryNumbers.Add(input[i].ToCharArray());
            }
        }
    }
}

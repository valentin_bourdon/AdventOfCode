﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace AdventOfCode2021
{
    public class Day_04 : BaseDay
    {
        public struct Board
        {
            public int[,] grid;
            public bool[,] marked;
            public int score;
            public bool win;
        }

        private List<Board> Boards;
        private string[] inputs;
        private List<int> randomNumbers;
        private List<int> boardsWon;
        private int boardWinner;
        int randomNumber = 0;


        public Day_04()
        {
            Boards = new List<Board>();
            randomNumbers = new List<int>();
            boardsWon = new List<int>();

            ReadInput();

            Bingo();
        }

        public override ValueTask<string> Solve_1()
        {
            return new(Boards[boardsWon.First()].score.ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            return new(Boards[boardsWon.Last()].score.ToString());
        }

        /// <summary>
        /// Bingo Game
        /// </summary>
        void Bingo()
        {
            for (int k = 0; k < randomNumbers.Count; k++)
            {
                randomNumber = GetRandomNumber(k);
                for (int w = 0; w < Boards.Count; w++)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        for (int j = 0; j < 5; j++)
                        {
                            if (Boards[w].grid[i, j] == randomNumber)
                            {
                                Boards[w].marked[i, j] = true;
                            }
                        }
                    }
                }

                IsBoardWin();
            }
        }

        /// <summary>
        /// Return the current number of this turn
        /// </summary>
        /// <param name="_startIndex"></param>
        /// <returns></returns>
        int GetRandomNumber(int _startIndex)
        {
            return randomNumbers[_startIndex];
        }

        /// <summary>
        /// Check if a board win at this turn
        /// </summary>
        void IsBoardWin()
        {
            //Check by row
            if (WinByRow())
            {
                if (!Boards[boardWinner].win)
                {
                    boardsWon.Add(boardWinner);

                    Board temp = Boards[boardWinner];
                    temp.win = true;
                    temp.score = GetScore(boardWinner, randomNumber);
                    Boards[boardWinner] = temp;
                }
            }

            //Check by column
            if (WinByColumn())
            {
                if (!Boards[boardWinner].win)
                {
                    boardsWon.Add(boardWinner);

                    Board temp = Boards[boardWinner];
                    temp.win = true;
                    temp.score = GetScore(boardWinner, randomNumber);
                    Boards[boardWinner] = temp;
                }
            }
        }

        /// <summary>
        /// Determine is the board win by completed a row
        /// </summary>
        /// <returns></returns>
        bool WinByRow()
        {
            bool isBoardWin = false;
            boardWinner = 0;

            //Check by row
            for (int k = 0; k < Boards.Count; k++)
            {
                for (int i = 0; i < 5; i++)
                {
                    isBoardWin = true;

                    for (int j = 0; j < 5; j++)
                    {
                        if (Boards[k].marked[i, j] == false)
                            isBoardWin = false;
                    }

                    //Found a winning board (not already won)
                    if (!Boards[k].win && isBoardWin)
                    {
                        boardWinner = k;
                        return isBoardWin;
                    }
                }
            }

            return false;

        }

        /// <summary>
        /// Determine is the board win by completed a column
        /// </summary>
        /// <returns></returns>
        bool WinByColumn()
        {
            bool isBoardWin = true;
            boardWinner = 0;

            //Check by column
            for (int k = 0; k < Boards.Count; k++)
            {
                for (int i = 0; i < 5; i++)
                {
                    isBoardWin = true;

                    for (int j = 0; j < 5; j++)
                    {
                        if (Boards[k].marked[j, i] == false)
                            isBoardWin = false;
                    }

                    //Found a winning board (not already won)
                    if (!Boards[k].win && isBoardWin)
                    {
                        boardWinner = k;
                        return isBoardWin;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Return the score of the board
        /// </summary>
        /// <param name="_boardWinner"></param>
        /// <param name="_lastNumber"></param>
        /// <returns></returns>
        private int GetScore(int _boardWinner, int _lastNumber)
        {
            int score = 0;
            for (int i = 0; i < 5; i++)
            {  
                for (int j = 0; j < 5; j++)
                {
                    if (Boards[_boardWinner].marked[i, j] == false)
                    {
                        score += Boards[_boardWinner].grid[i, j];
                    }
                }
            }

            return score * _lastNumber;
        }

        /// <summary>
        /// Create boards
        /// </summary>
        /// <param name="_boardsValues"></param>
        private void InitBoards(List<string[]> _boardsValues)
        {
            for (int i = 0; i < _boardsValues.Count; i++)
            {
                Board boardItem = new Board();
                boardItem.grid = new int[5, 5];
                boardItem.marked = new bool[5, 5];
                boardItem.win = false;
                int column = 0;

                for (int j = 0; j < _boardsValues[i].Length; j++)
                {
                    column = 0;
                    foreach (var item in _boardsValues[i][j].Split(' ', StringSplitOptions.RemoveEmptyEntries))
                    {
                        boardItem.grid[j, column] = int.Parse(item);
                        boardItem.marked[j, column] = false;
                        column++;
                    }
                }
                Boards.Add(boardItem);

            }
        }

        private void ReadInput()
        {
            inputs = File.ReadAllLines(InputFilePath);
            string[] lines = new string[5];
            List<string[]> boardsValues = new List<string[]>();
            int lineIndex = 0;

            for (int i = 0; i < inputs.Length; i++)
            {
                foreach (var line in inputs[i].Split('\n'))
                {
                    if (line.Contains(','))
                    {
                        foreach (var item in line.Split(','))
                            randomNumbers.Add(int.Parse(item));
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(line))
                        {
                            lines[lineIndex] = line.Trim();
                            lineIndex++;

                            if(lineIndex == 4)
                                boardsValues.Add(lines);
                        }
                        else
                        {
                            lineIndex = 0;
                            lines = new string[5];
                        }
                    }   
                }
            }

            InitBoards(boardsValues);
        }
    }
}

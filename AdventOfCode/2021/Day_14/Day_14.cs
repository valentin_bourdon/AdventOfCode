﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace AdventOfCode2021
{
    public class Day_14 : BaseDay
    {
        string[] inputs;
        string polymerTemplate;
        Dictionary<string, char> pairs;

        public Day_14()
        {
            pairs = new Dictionary<string, char>();

            ReadInput();
        }

        public override ValueTask<string> Solve_1()
        {
            //SolvePairInsertionRule(10);
            //Dictionary<char, Int64> polymerDict = DictionnaryBasedOnPolymer();

            Dictionary<string, Int64> polymerDict = SolvePolymer(10);
            Dictionary<char, Int64> dictCharPolymer = DictionaryCharPolymer(polymerDict);

            Int64 maxValue = dictCharPolymer.Values.Max();
            Int64 minValue = dictCharPolymer.Values.Min();

            return new((maxValue - minValue).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            Dictionary<string, Int64> polymerDict = SolvePolymer(40);
            Dictionary<char, Int64> dictCharPolymer = DictionaryCharPolymer(polymerDict);

            Int64 maxValue = dictCharPolymer.Values.Max();
            Int64 minValue = dictCharPolymer.Values.Min();

            return new((maxValue - minValue).ToString());
        }

        /// <summary>
        /// Create a new polymer string based on pairs
        /// Have some limitation to create a huge polymer
        /// It's not optimised solution
        /// </summary>
        /// <param name="_step"></param>
        private void SolvePairInsertionRule(int _step)
        {
            Dictionary<int, char> insertChars = new Dictionary<int, char>();
            string pair = string.Empty;
            char insertChar;
            int insertIndex = 0;

            for (int i = 0; i < _step; i++)
            {
                insertChars.Clear();
                insertIndex = 0;

                for (int j = 0; j < polymerTemplate.Length; j++)
                {
                    //Insert first char of the pair
                    insertChars.Add(insertIndex, polymerTemplate[j]);
                    insertIndex++;

                    //Try to find pair insertion
                    pair = string.Empty;

                    if (j + 1 < polymerTemplate.Length)
                    {
                        //Get the pair insertion
                        pair += polymerTemplate[j];
                        pair += polymerTemplate[j + 1];

                        //Get the char to insert between the pair 
                        if (pairs.TryGetValue(pair, out insertChar))
                        {
                            insertChars.Add(insertIndex, insertChar);//Insert second char
                            insertIndex++;
                        }
                    }
                }

                //Writre new polymer
                polymerTemplate = string.Empty;
                foreach (var item in insertChars)
                    polymerTemplate += item.Value;
            }
        }

        /// <summary>
        /// Store in dictionary all pairs of the poylmer
        /// More optimised for huge polymer
        /// </summary>
        /// <param name="_numberOfTep"></param>
        /// <returns></returns>
        private Dictionary<string, Int64> SolvePolymer(int _numberOfTep)
        {
            //Start by creating a dict containing pairs of the polymer template
            Dictionary<string, Int64> polymerDictTemplate = new Dictionary<string, Int64>();

            for (int i = 0; i < polymerTemplate.Length - 1; i++)
            {
                if (!polymerDictTemplate.ContainsKey(string.Concat(polymerTemplate[i], polymerTemplate[i + 1])))
                    polymerDictTemplate.Add(string.Concat(polymerTemplate[i], polymerTemplate[i + 1]), 1);
                else
                    polymerDictTemplate[string.Concat(polymerTemplate[i], polymerTemplate[i + 1])]++;
            }

            //Loop arround step to create new polymer based on previous pairs
            Dictionary<string, Int64> newPolymerDict = new Dictionary<string, Int64>();

            for (int step = 0; step < _numberOfTep; step++)
            {
                newPolymerDict = new Dictionary<string, Int64>();
                foreach (var item in polymerDictTemplate)
                {
                    //Add Key[0] + correspondance of the Key pair (ex: NN -> C // add key[0]=N + C)
                    if (!newPolymerDict.ContainsKey(string.Concat(item.Key[0], pairs[item.Key])))
                        newPolymerDict.Add(string.Concat(item.Key[0], pairs[item.Key]), item.Value);
                    else
                        newPolymerDict[string.Concat(item.Key[0], pairs[item.Key])] += item.Value;

                    //Addcorrespondance of the Key pair + Key[1](ex: NN -> C // add C + key[1]=N)
                    if (!newPolymerDict.ContainsKey(string.Concat(pairs[item.Key], item.Key[1])))
                        newPolymerDict.Add(string.Concat(pairs[item.Key], item.Key[1]), item.Value);
                    else
                        newPolymerDict[string.Concat(pairs[item.Key], item.Key[1])] += item.Value;
                }

                polymerDictTemplate = newPolymerDict;
            }

            return polymerDictTemplate;
        }


        /// <summary>
        /// Create a dictionary to organize separately all different character
        /// </summary>
        /// <returns></returns>
        private Dictionary<char, Int64> DictionnaryBasedOnPolymer()
        {
            Dictionary<char, Int64> _polymerDict = new Dictionary<char, Int64>();

            for (int i = 0; i < polymerTemplate.Length; i++)
            {
                if (!_polymerDict.ContainsKey(polymerTemplate[i]))
                    _polymerDict.Add(polymerTemplate[i], 1);
                else
                {
                    _polymerDict[polymerTemplate[i]] += 1;
                }
            }

            return _polymerDict;
        }


        /// <summary>
        /// Create a dictionary of chars based of the dictionnry of pairs of the polymer
        /// </summary>
        /// <param name="_polymerDict"></param>
        /// <returns></returns>
        private Dictionary<char, Int64> DictionaryCharPolymer(Dictionary<string, Int64> _polymerDict)
        {
            Dictionary<char, Int64> dictCharPolymer = new Dictionary<char, Int64>();

            foreach (var item in _polymerDict)
            {
                //We keep only the second char of the pair because the second one, egal to the first one on the next pair (ex: p1 = NB and p2 = BB =>  p1[1]=B=p2[0] --> it's the same)
                // so we don't need to count it twice.
                if (!dictCharPolymer.ContainsKey(item.Key[1]))
                    dictCharPolymer.Add(item.Key[1], item.Value);
                else
                    dictCharPolymer[item.Key[1]] += item.Value;
            }

            return dictCharPolymer;
        }

        private void ReadInput()
        {
            inputs = File.ReadAllLines(InputFilePath);
            polymerTemplate = inputs[0];

            for (int i = 2; i < inputs.Length; i++)
            {
                string[] values = inputs[i].Split(" -> ", StringSplitOptions.RemoveEmptyEntries);
                pairs.Add(values[0], char.Parse(values[1]));
            }
        }
    }
}

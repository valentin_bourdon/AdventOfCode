﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace AdventOfCode2021
{
    public class Day_06 : BaseDay
    {
        string inputs;
        List<int> lanternfish;
        Dictionary<int, long> fishs;

        public Day_06()
        {
            lanternfish = new List<int>();
            fishs = new Dictionary<int, long>();

            ReadInput();
        }

        public override ValueTask<string> Solve_1()
        {
           
            return new(LanterFishPop(80, fishs).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            return new(LanterFishPop(256, fishs).ToString());
        }

        long LanterFishPop(int _numberOfDay, Dictionary<int,long> _fishs)
        {       
            for (int i = 0; i < _numberOfDay; i++)
            {
                Dictionary<int, long> newFish = new Dictionary<int, long>();
                foreach (var item in _fishs.Keys)
                {
                    if (item == 0)
                    {
                        if (!newFish.ContainsKey(6))
                            newFish.Add(6, _fishs[item]);
                        else
                            newFish[6] += _fishs[item];

                        if (!newFish.ContainsKey(8))
                            newFish.Add(8, _fishs[item]);
                        else
                            newFish[8] += _fishs[item];
                    }
                    else
                    {
                        if (!newFish.ContainsKey(item - 1))
                            newFish.Add(item - 1, _fishs[item]);
                        else
                            newFish[item - 1] += _fishs[item];
                    }
                }
                _fishs = newFish;
            }

            return _fishs.Sum(x => x.Value);
        }
        private void ReadInput()
        {
            inputs = File.ReadAllText(InputFilePath);
            lanternfish = inputs.Split(',').Select(Int32.Parse).ToList();


            for (int i = 0; i < lanternfish.Count; i++)
            {
                if (fishs.ContainsKey(lanternfish[i]))
                {
                    fishs[lanternfish[i]] += 1;
                }
                else
                    fishs.Add(lanternfish[i], 1);
            }
        }
    }
}

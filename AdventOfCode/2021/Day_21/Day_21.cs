﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace AdventOfCode2021
{
    public class Day_21 : BaseDay
    {
        string[] inputs;

        //Rolls
        const int maxRolls = 100;
        const int numberOfRollsPerTurn = 3;
        int numberOfRoll = 0;
        List<int> possibleRolls;

        //Players
        private List<Player> players;

        //Universes
        Dictionary<(int currentPlayerPosition, int currentPlayerScore, int nextPlayerPosition, int nextPlayerScore), (long, long)> universes;

        public Day_21()
        {
            players = new List<Player>();
            possibleRolls = new List<int>();
            universes = new Dictionary<(int currentPlayerPosition, int currentPlayerScore, int nextPlayerPosition, int nextPlayerScore), (long, long)>();

            //Get all possible rolls
            List<int> num = new List<int>() { 1, 2, 3 };
            var permut = GetPermutationsWithRept(num, 3).ToList();
            for (int i = 0; i < permut.Count; i++)
            {
                var possibility = permut[i].ToList();
                possibleRolls.Add(possibility.Sum());
            }

            ReadInput();
        }

        public override ValueTask<string> Solve_1()
        {
            CreatePlayers();
            DeterministicDiracDiceGame(1000);
            int result = players.Select(score => score.Score).Min() * numberOfRoll;

            return new(result.ToString());
        }


        public override ValueTask<string> Solve_2()
        {
            CreatePlayers();
            var (score1, score2) = QuantumDiracDiceGame(players[0].Position - 1, 0, players[1].Position - 1, 0);
            var result = Math.Max(score1, score2);

            return new(result.ToString());
        }


        private void ReadInput()
        {
            inputs = File.ReadAllLines(InputFilePath);
        }

        /// <summary>
        /// Play on deterministe game
        /// </summary>
        /// <param name="_score"></param>
        private void DeterministicDiracDiceGame(int _score)
        {
            int rool = 1;
            numberOfRoll = 0;
            while (players.All(score => score.Score < _score))
            {
                for (int i = 0; i < players.Count; i++)
                {
                    int playerPosition = 0;
                    for (int j = 0; j < numberOfRollsPerTurn; j++)
                    {
                        playerPosition += rool + j;
                        numberOfRoll++;
                    }

                    players[i].Score += players[i].MovePlayer(playerPosition);

                    if (players[i].Score >= _score)
                        break;

                    rool += numberOfRollsPerTurn;
                    if (numberOfRollsPerTurn >= maxRolls)
                        rool = 1;
                }
            }
        }

        /// <summary>
        /// Play on quantum game
        /// </summary>
        /// <param name="currentPlayerPosition"></param>
        /// <param name="currentPlayerScore"></param>
        /// <param name="nextPlayerPosition"></param>
        /// <param name="nextPlayerScore"></param>
        /// <returns></returns>
        private (long _nbUniversePlayer_1, long _nbUniversePlayer_2) QuantumDiracDiceGame(int currentPlayerPosition, int currentPlayerScore, int nextPlayerPosition, int nextPlayerScore)
        {
            //If on of the player have 21 or more point in that universe
            //Return 1 to the winner
            //Return 0 to the looser
            if (currentPlayerScore >= 21 || nextPlayerScore >= 21)
            {
                if (currentPlayerScore >= 21)
                    return (1, 0);
                else if (nextPlayerScore >= 21)
                    return (0, 1);
            }


            if (!universes.ContainsKey((currentPlayerPosition, currentPlayerScore, nextPlayerPosition, nextPlayerScore)))
            {
                
                var (x, y) = (0L, 0L);
              
                foreach (var roll in possibleRolls)
                {
                    var newPlayerPosition = (currentPlayerPosition + roll) % 10;
                    var (i, j) = QuantumDiracDiceGame(nextPlayerPosition, nextPlayerScore, newPlayerPosition, currentPlayerScore + newPlayerPosition + 1);

                    //Increment the number of universe won by each player's
                    x += j;
                    y += i;

                    universes[(currentPlayerPosition, currentPlayerScore, nextPlayerPosition, nextPlayerScore)] = (x, y);
                }
            }
            return universes[(currentPlayerPosition, currentPlayerScore, nextPlayerPosition, nextPlayerScore)];
        }
       
        private void CreatePlayers()
        {
            players = new List<Player>();
            //Create players
            for (int i = 0; i < inputs.Length; i++)
            {
                Player player = new Player(int.Parse(inputs[i].Split(':')[1]));
                players.Add(player);
            }
        }


        public class Player
        {
            public Player(int _startPosition)
            {
                Position = _startPosition;
                Score = 0;
            }

            private int position;
            public int Position
            {
                get => position;
                set => position = value;
            }

            private int score;
            public int Score
            {
                get => score;
                set => score = value;
            }

            public int MovePlayer(int _moves)
            {
                for (int i = 0; i < _moves; i++)
                {
                    Position += 1;
                    if (Position > 10)
                        Position = 1;
                }

                return Position;
            }
        }

        static IEnumerable<IEnumerable<T>> GetPermutationsWithRept<T>(IEnumerable<T> list, int length)
        {
            if (length == 1) return list.Select(t => new T[] { t });
            return GetPermutationsWithRept(list, length - 1)
                .SelectMany(t => list,
                    (t1, t2) => t1.Concat(new T[] { t2 }));
        }

    }
}


## 2021 Puzzles List **11 / 25** (44%) -- WORK IN PROGRESS

- [Day 1](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_01) 
- [Day 2](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_02)
- [Day 3](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_03) 
- [Day 4](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_04) 
- [Day 5](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_05)
- [Day 6](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_06)
- [Day 7](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_07)
- [Day 8](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_08)
- [Day 9](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_09)
- [Day 10](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_10)
- [Day 11](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_11)
- [Day 13](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_13)
- [Day 14](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/Day_14)


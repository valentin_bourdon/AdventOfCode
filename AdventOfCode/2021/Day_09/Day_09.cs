﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Collections;

namespace AdventOfCode2021
{
    public class Day_09 : BaseDay
    {
        string[] inputs;
        private Dictionary<int, List<int>> heigthmap;
        private List<int> lowPoints;
        private List<Tuple<int,int>> lowPointsCoordinates;


        public Day_09()
        {
            heigthmap = new Dictionary<int, List<int>>();
            lowPoints = new List<int>();
            lowPointsCoordinates = new List<Tuple<int, int>>();

            ReadInput();
        }

        public override ValueTask<string> Solve_1()
        {
            for (int i = 0; i < heigthmap.Count; i++)
            {
                bool isLowerPoint = true;
                for (int j = 0; j < heigthmap[i].Count; j++)
                {
                    isLowerPoint = true;
                    if (i > 0 && CheckUp(i, j))
                        isLowerPoint = false;

                    if (i < heigthmap.Count - 1 && CheckDown(i, j))
                        isLowerPoint = false;

                    if (j > 0 && CheckLeft(i, j))
                        isLowerPoint = false;

                    if (j < heigthmap[i].Count - 1 && CheckRight(i, j))
                        isLowerPoint = false;

                    if (isLowerPoint)
                    {
                        lowPoints.Add(heigthmap[i][j]);
                        lowPointsCoordinates.Add(new Tuple<int, int>(i, j));
                    }
                }
            }


            return new(lowPoints.Select((x, i) => (x += 1)).Sum().ToString());
        }



        public override ValueTask<string> Solve_2()
        {
            Dictionary<int, List<int>> bassins = new Dictionary<int, List<int>>();

            for (int i = 0; i < lowPointsCoordinates.Count; i++)
            {
                Bassin bassin = new Bassin();
                bassins.Add(i,bassin.SolveBassin(heigthmap, lowPointsCoordinates[i]));
            }

            //Sort bassin by order (Max to min)
            var sortBassin = bassins.ToList();
            sortBassin.Sort((pair1, pair2) => pair2.Value.Count.CompareTo(pair1.Value.Count));


            //Get only the first largest bassin
            int result = sortBassin[0].Value.Count * sortBassin[1].Value.Count * sortBassin[2].Value.Count;
           

            return new(result.ToString());

        }

        public class Bassin
        {
            public List<int> SolveBassin(Dictionary<int, List<int>> heightMap, Tuple<int, int> startPosition)
            {
                List<int> pointInBassin = new List<int>();

                // Directions
                int[,] dir = { { 0, 1 }, { 0, -1 },
                   { 1, 0 }, { -1, 0 } };

                // Queue
                Queue q = new Queue();

                // Insert the first point coordinate
                q.Enqueue(startPosition);
                pointInBassin.Add(heightMap[startPosition.Item1][startPosition.Item2]);

                // Until queue is empty
                while (q.Count > 0)
                {
                    Tuple<int, int> p = (Tuple<int, int>)(q.Peek());
                    q.Dequeue();

                    // Mark as visited
                    heightMap[p.Item1][p.Item2] = 9;

                    // Check all four directions
                    for (int i = 0; i < 4; i++)
                    {

                        // Using the direction array
                        int a = p.Item1 + dir[i, 0];
                        int b = p.Item2 + dir[i, 1];

                        // Not blocked and valid
                        if (a >= 0 && b >= 0 &&
                            a < heightMap.Count && b < heightMap[a].Count &&
                              heightMap[a][b] != 9)
                        {

                            Tuple<int, int> temp = new Tuple<int, int>(a, b);
                            if (!q.Contains(temp))
                            {
                                pointInBassin.Add(heightMap[a][b]);
                                q.Enqueue(new Tuple<int, int>(a, b));
                            }
                        }
                    }
                }

                return pointInBassin;
            }
        }


        /// <summary>
        /// Check the value on up [0,1]
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private bool CheckUp(int row, int column)
        {
            if (heigthmap[row - 1][column] <= heigthmap[row][column])
                return true;
            else
                return false;
        }

        /// <summary>
        /// Check the value on down [0,-1]
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private bool CheckDown(int row, int column)
        {
            if (heigthmap[row + 1][column] <= heigthmap[row][column])
                return true;
            else
                return false;
        }

        /// <summary>
        /// Check the value on right [1,0]
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private bool CheckRight(int row, int column)
        {
            if (heigthmap[row][column + 1] <= heigthmap[row][column])
                return true;
            else
                return false;

        }

        /// <summary>
        /// Check the value on left [-1,0]
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private bool CheckLeft(int row, int column)
        {
            if (heigthmap[row][column - 1] <= heigthmap[row][column])
                return true;
            else
                return false;
        }

        private void ReadInput()
        {
            inputs = File.ReadAllLines(InputFilePath);
            

            for (int i = 0; i < inputs.Length; i++)
            {
                List<int> values = inputs[i].Select(x => int.Parse(x.ToString())).ToList();
                heigthmap.Add(i, values);
            }
        }
    }
}

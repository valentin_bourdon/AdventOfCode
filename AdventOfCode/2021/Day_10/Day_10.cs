﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Collections;

namespace AdventOfCode2021
{
    public class Day_10 : BaseDay
    {
        string[] inputs;

        private Dictionary<char, int> syntaxErrorTable = new Dictionary<char, int>
        {
            [')'] = 3,
            [']'] = 57,
            ['}'] = 1197,
            ['>'] = 25137,

        };

        private Dictionary<char, int> syntaxCompleteTable = new Dictionary<char, int>
        {
            [')'] = 1,
            [']'] = 2,
            ['}'] = 3,
            ['>'] = 4,

        };

        private char[] closingCharacters = new char[]
       {
            ')',
            ']',
            '}',
            '>',
       };

        private Dictionary<int, List<char>> navigationSubsystem;
        private List<char> illegalCharacters;
        private List<char> charactersOpened = new List<char>();
        private Dictionary<int, List<char>> incompletLines;
        private List<long> completionLinesScore;

        public Day_10()
        {
            navigationSubsystem = new Dictionary<int, List<char>>();
            illegalCharacters = new List<char>();
            incompletLines = new Dictionary<int, List<char>>();
            completionLinesScore = new List<long>();

            ReadInput();
            SolvedNavigationSubsytem();
        }

        public override ValueTask<string> Solve_1()
        {
            return new(GetSyntaxErrorScore().ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            return new(GetCompleteLinesCore().ToString());
        }

        private void SolvedNavigationSubsytem()
        {
            bool chunkIsClose = false;
            for (int i = 0; i < navigationSubsystem.Count; i++)
            {
                chunkIsClose = false;
                charactersOpened.Clear();

                for (int j = 0; j < navigationSubsystem[i].Count; j++)
                {
                    if (closingCharacters.Contains(navigationSubsystem[i][j]))
                    {
                        chunkIsClose = false;

                        switch (navigationSubsystem[i][j])
                        {
                            case ')':
                                if (charactersOpened.Contains('('))
                                    chunkIsClose = IsChunkClosed('(');
                                break;
                            case ']':
                                if (charactersOpened.Contains('['))
                                    chunkIsClose = IsChunkClosed('[');
                                break;
                            case '}':
                                if (charactersOpened.Contains('{'))
                                    chunkIsClose = IsChunkClosed('{');
                                break;
                            case '>':
                                if (charactersOpened.Contains('<'))
                                    chunkIsClose = IsChunkClosed('<');
                                break;
                            default:
                                break;
                        }

                        if (!chunkIsClose)
                        {
                            illegalCharacters.Add(navigationSubsystem[i][j]);

                            charactersOpened.Clear();

                            //Break loop - Go to next row
                            if (i < navigationSubsystem.Count - 1)
                                j = navigationSubsystem[i].Count - 1;
                            else
                                break;
                        }
                    }
                    else
                        charactersOpened.Add(navigationSubsystem[i][j]);
                }

                if (charactersOpened.Count > 0)
                    CompleteLine(charactersOpened);
            }
        }

        /// <summary>
        /// Solved incomplet lines
        /// </summary>
        /// <param name="_charToComplete"></param>
        private void CompleteLine(List<char> _charToComplete)
        {
            _charToComplete.Reverse();
            List<char> _chars = new List<char>();
            for (int i = 0; i< _charToComplete.Count; i++)
            {
                switch (_charToComplete[i])
                {
                    case '(':
                        _chars.Add(')');
                        break;
                    case '[':
                        _chars.Add(']');
                        break;
                    case '{':
                        _chars.Add('}');
                        break;
                    case '<':
                        _chars.Add('>');
                        break;
                    default:
                        break;
                }
            }

            incompletLines.Add(incompletLines.Count, _chars);
        }

        /// <summary>
        /// Score the completion strings, and sort the scores and get the middle one
        /// </summary>
        /// <returns></returns>
        private double GetCompleteLinesCore()
        {
            long totalPoints = 0;
            int points = 0;
            for (int i = 0; i < incompletLines.Count; i++)
            {
                totalPoints = 0;
                for (int j = 0; j < incompletLines[i].Count; j++)
                {
                    if (syntaxCompleteTable.TryGetValue(incompletLines[i][j], out points))
                        totalPoints = totalPoints * 5 + points;
                }

                completionLinesScore.Add(totalPoints);
            }


            return Median();
        }

        /// <summary>
        /// Get the Median
        /// </summary>
        /// <returns></returns>
        double Median()
        {
            int numberCount = completionLinesScore.Count();
            int halfIndex = completionLinesScore.Count() / 2;
            var sortedNumbers = completionLinesScore.OrderBy(n => n);
            
            double median;
            if ((numberCount % 2) == 0)
            {
                median = ((sortedNumbers.ElementAt(halfIndex) +
                    sortedNumbers.ElementAt((halfIndex - 1))) / 2);
            }
            else
            {
                median = sortedNumbers.ElementAt(halfIndex);
            }

            return median;

        }

        /// <summary>
        /// Get the syntax error score
        /// </summary>
        /// <returns></returns>
        private int GetSyntaxErrorScore()
        {
            List<int> totalPoints = new List<int>();
            int points;
            for (int i = 0; i < illegalCharacters.Count; i++)
            {
                if (syntaxErrorTable.TryGetValue(illegalCharacters[i], out points))
                    totalPoints.Add(points);
            }

            return totalPoints.Sum();
        }

        /// <summary>
        /// Determine is the chunk is corrupted or not
        /// </summary>
        /// <param name="_char">Char to find</param>
        /// <returns></returns>
        private bool IsChunkClosed(char _char)
        {
            int index = charactersOpened.LastIndexOf(_char);
            if ((charactersOpened.Count - index) == 1)// Means all sequences are closed between them
            {
                charactersOpened.RemoveAt(index);
                return true;
            }

            return false;
        }

        private void ReadInput()
        {
            inputs = File.ReadAllLines(InputFilePath);

            for (int i = 0; i < inputs.Length; i++)
            {
                List<char> values = inputs[i].Select(x => x).ToList();
                navigationSubsystem.Add(i, values);
            }
        }
    }
}

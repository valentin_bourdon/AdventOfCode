﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace AdventOfCode2021
{
    public class Day_08 : BaseDay
    {
        string[] inputs;
        private Dictionary<int, List<string>> signalPatterns;
        private Dictionary<int, List<string>> outputValues;

        string[] digit = new string[10]
        {
            "abcefg",
            "cf",
            "acdeg",
            "acdfg",
            "bcdf",
            "abdfg",
            "abdefg",
            "acf",
            "abcdefg",
            "abcdfg",
        };


        public Day_08()
        {
            signalPatterns = new Dictionary<int, List<string>>();
            outputValues = new Dictionary<int, List<string>>();

            ReadInput();         
        }

        public override ValueTask<string> Solve_1()
        {
            int count = 0;
            for (int i = 0; i < signalPatterns.Count; i++)
            {
                for (int j = 0; j < digit.Length; j++)
                {
                    if (j == 1 || j == 4 || j == 7 || j == 8)
                    {
                        int numberOfSegments = digit[j].Length;

                        foreach (var pattern in signalPatterns[i])
                        {
                            if (pattern.Length == numberOfSegments)
                            {
                                foreach (var output in outputValues[i])
                                {
                                    if (output.Length == pattern.Length)
                                    {
                                        if (IsContainsChar(pattern, output))
                                        {
                                            count += 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return new(count.ToString());
           
        }

        public override ValueTask<string> Solve_2()
        {
            int totalSum = 0;
            int sum = 0;
            Dictionary<int, List<string>> _patterns = signalPatterns;
            string[] _valuesToDecode;

            for (int i = 0; i < _patterns.Count; i++)
            {
                _valuesToDecode = new string[10];

                //FIND 1, 4, 7, 8
                for (int j = _patterns[i].Count - 1; j >= 0; j--)
                {
                    switch (_patterns[i][j].Length)
                    {
                        case 2://1
                            _valuesToDecode[1] = signalPatterns[i][j];
                            _patterns[i].RemoveAt(j);
                            break;
                        case 3://7
                            _valuesToDecode[7] = signalPatterns[i][j];
                            _patterns[i].RemoveAt(j);
                            break;
                        case 4://4
                            _valuesToDecode[4] = signalPatterns[i][j];
                            _patterns[i].RemoveAt(j);
                            break;
                        case 7://8
                            _valuesToDecode[8] = signalPatterns[i][j];
                            _patterns[i].RemoveAt(j);
                            break;
                        default:
                            break;
                    }
                }

                //FIND 0, 6, 9
                for (int j = _patterns[i].Count - 1; j >= 0; j--)
                {
                    switch (_patterns[i][j].Length)
                    {
                        case 6:
                            if (IsContainsChar(_patterns[i][j], _valuesToDecode[4]))
                            {
                                _valuesToDecode[9] = _patterns[i][j];
                                _patterns[i].RemoveAt(j);
                            }
                            else if (!IsContainsChar(_patterns[i][j], _valuesToDecode[1]))
                            {
                                _valuesToDecode[6] = _patterns[i][j];
                                _patterns[i].RemoveAt(j);
                            }
                            else
                            {
                                _valuesToDecode[0] = _patterns[i][j];
                                _patterns[i].RemoveAt(j);
                            }
                            break;
                        default:
                            break;
                    }
                }

                //FIND 2, 3, 5
                for (int j = _patterns[i].Count - 1; j >= 0; j--)
                {
                    switch (_patterns[i][j].Length)
                    {
                        case 5:
                            if (IsContainsChar(_patterns[i][j], _valuesToDecode[1]))
                            {
                                _valuesToDecode[3] = _patterns[i][j];
                                _patterns[i].RemoveAt(j);
                            }
                            else if (IsContainsChar(_valuesToDecode[6], _patterns[i][j]))
                            {
                                _valuesToDecode[5] = _patterns[i][j];
                                _patterns[i].RemoveAt(j);
                            }
                            else
                            {
                                _valuesToDecode[2] = _patterns[i][j];
                                _patterns[i].RemoveAt(j);
                            }
                            break;
                        default:
                            break;
                    }
                }

               //Calculate sum
                sum = 0;
                for (int j = 0; j < outputValues[i].Count; j++)
                {
                    sum = sum * 10 + DigitDecoding(_valuesToDecode, outputValues[i][j]);
                }

                totalSum += sum;
            }
            
            return new(totalSum.ToString()); 
        }

        /// <summary>
        /// Decode the digit output value
        /// </summary>
        /// <param name="_valuesToDecode"></param>
        /// <param name="_outputValue"></param>
        /// <returns></returns>
        private int DigitDecoding(string[] _valuesToDecode, string _outputValue)
        {
            for (int i = 0; i < _valuesToDecode.Length; i++)
            {
                if (_outputValue.Length == _valuesToDecode[i].Length && IsContainsChar(_outputValue, _valuesToDecode[i]))
                {
                    return i;
                }
            }

            throw new Exception("Error to fin digit decoding");
        }

        /// <summary>
        /// Check if a string contains all character of another one
        /// </summary>
        /// <param name="_strToCheck">String to analyse</param>
        /// <param name="_strInput">String input</param>
        /// <returns></returns>
        private  static bool IsContainsChar(string _strToCheck, string _strInput)
        {
            foreach (char c in _strInput)
            {
                if (!_strToCheck.Contains(c))
                    return false;
            }

            return true;
        }

        private void ReadInput()
        {
            inputs = File.ReadAllLines(InputFilePath);

            for (int i = 0; i < inputs.Length; i++)
            {
               
                string[] _signal = inputs[i].Split('|');

                List<string> _patterns = _signal[0].Split(' ',StringSplitOptions.RemoveEmptyEntries).ToList();
                List<string> _outputs = _signal[1].Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList();

                signalPatterns.Add(i, _patterns);
                outputValues.Add(i, _outputs);
            }
        }
    }
}

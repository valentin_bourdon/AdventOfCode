﻿using AoCHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



if (args.Length == 0)
{
    var indexes = args.Select(arg => uint.TryParse(arg, out var index) ? index : uint.MaxValue);
    IEnumerable<uint> problemNumber = new List<uint>() { 21 };
    Solver.Solve(problemNumber);

    //Solver.SolveLast();
}
else if (args.Length == 1 && args[0].Contains("all", StringComparison.CurrentCultureIgnoreCase))
{
    Solver.SolveAll();
}
else
{
    var indexes = args.Select(arg => uint.TryParse(arg, out var index) ? index : uint.MaxValue);

    Solver.Solve(indexes.Where(i => i < uint.MaxValue));
}
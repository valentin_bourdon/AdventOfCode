﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace AdventOfCode2021
{
    public class Day_13 : BaseDay
    {
        string[] inputs;

        public static List<Instruction> instructions;
        public TransparentPaper transparentPaper;

        public Day_13()
        {
            instructions = new List<Instruction>();

            ReadInput();
        }

        public override ValueTask<string> Solve_1()
        {
            TransparentPaper transparentPaper = new TransparentPaper(inputs);
            transparentPaper.SolveTransparentPaper(1);

            return new(transparentPaper.NumberOfDots().ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            TransparentPaper transparentPaper = new TransparentPaper(inputs);
            transparentPaper.SolveTransparentPaper(instructions.Count);

            transparentPaper.WriteCode();//Need to wrtie code in console

            return new("The code appears in console!");
        }


        private void ReadInput()
        {
            inputs = File.ReadAllLines(InputFilePath);

            for (int i = 0; i < inputs.Length; i++)
            {
                if (inputs[i].Contains("fold along"))
                {
                    if (inputs[i].Contains("y"))
                    {
                        string value = inputs[i].Split('=').ElementAt(1);
                        instructions.Add(new Instruction("y", int.Parse(value)));
                    }
                    else if (inputs[i].Contains("x"))
                    {
                        string value = inputs[i].Split('=').ElementAt(1);
                        instructions.Add(new Instruction("x", int.Parse(value)));
                    }
                }
            }
        }

        public class TransparentPaper{

            public struct Coordinate
            {
                public int x;
                public int y;
            }
            public List<Coordinate> coordinates;

            private string[][] grid;

            private int numberOfColumn;//x
            private int numberOfRow;//y

            public TransparentPaper(string[] _inputs)
            {
                coordinates = new List<Coordinate>();
                GetCoordinates(_inputs);
                ConstructGrid();
            }

            private void GetCoordinates(string[] _inputs)
            {
                for (int i = 0; i < _inputs.Length; i++)
                {
                    if (!string.IsNullOrEmpty(_inputs[i]) && _inputs[i].Contains(','))
                    {
                        string[] _coordinates = _inputs[i].Split(",");
                        Coordinate coordinate = new Coordinate();
                        coordinate.x = int.Parse(_coordinates[0]);
                        coordinate.y = int.Parse(_coordinates[1]);

                        coordinates.Add(coordinate);
                    }
                }
            }

            private void ConstructGrid()
            {
                numberOfColumn = coordinates.Max(coordinate => coordinate.x) + 1;
                numberOfRow = coordinates.Max(coordinate => coordinate.y) + 1;

                grid = new string[numberOfRow][];

                // Init transparent paper with dots
                for (int row = 0; row < numberOfRow; row++)
                {
                    grid[row] = new string[numberOfColumn];
                    for (int column = 0; column < numberOfColumn; column++)
                        grid[row][column] = ".";
                }

                //Adjust by pattern '#' on coordinates
                for (int coordinate = 0; coordinate < coordinates.Count; coordinate++)
                    grid[coordinates[coordinate].y][coordinates[coordinate].x] = "#";
            }

            public void SolveTransparentPaper(int _numberOfInstructions)
            {
                string[][] transparentPaperSolved = new string[][] { };

                for (int instruction = 0; instruction < _numberOfInstructions; instruction++)
                {
                    int line = 0;
                    switch (instructions[instruction].coordinate)
                    {
                        case "y":
                            line = instructions[instruction].value;
                            transparentPaperSolved = new string[line][];

                            //Init paper solved from 0 to line instruction
                            for (int row = 0; row < line; row++)
                            {
                                transparentPaperSolved[row] = new string[grid[0].Length];
                                for (int column = 0; column < grid[row].Length; column++)
                                    transparentPaperSolved[row][column] = grid[row][column];
                            }

                            //Fold the paper up
                            for (int row = grid.Length - 1; row > line; row--)
                            {
                                for (int column = 0; column < grid[row].Length; column++)
                                {
                                    if (transparentPaperSolved[(grid.Length - 1) - row][column] != "#")
                                        transparentPaperSolved[(grid.Length - 1) - row][column] = grid[row][column];
                                }
                            }

                            break;
                        case "x":
                            line = instructions[instruction].value;
                            transparentPaperSolved = new string[grid.Length][];

                            //Init paper solved from 0 to line instruction
                            for (int row = 0; row < grid.Length; row++)
                            {
                                transparentPaperSolved[row] = new string[line];
                                for (int column = 0; column < line; column++)
                                    transparentPaperSolved[row][column] = grid[row][column];
                            }

                            //Fold the paper left
                            for (int row = 0; row < grid.Length; row++)
                            {
                                for (int column = grid[row].Length - 1; column > line; column--)
                                {
                                    if (transparentPaperSolved[row][(grid[row].Length - 1) - column] != "#")
                                        transparentPaperSolved[row][(grid[row].Length - 1) - column] = grid[row][column];
                                }
                            }

                            break;
                        default:
                            break;
                    }

                    grid = transparentPaperSolved.ToArray();
                }
            }


            public int NumberOfDots()
            {
                int numberOfDots = 0;

                for (int row = 0; row < grid.Length; row++)
                {
                    for (int column = 0; column < grid[row].Length; column++)
                    {
                        if (grid[row][column] == "#")
                            numberOfDots++;
                    }
                }

                return numberOfDots;
            }

            public void WriteCode()
            {
                string result = string.Empty;
                for (int row = 0; row < grid.Length; row++)
                {
                    result = string.Empty;
                    for (int column = 0; column < grid[row].Length; column++)
                    {
                        result += grid[row][column];
                    }
                    Console.WriteLine(result);
                }
            }
        }

        public class Instruction
        {
            public string coordinate;
            public int value;

            public Instruction(string _coordinate, int _value)
            {
                coordinate = _coordinate;
                value = _value;
            }
        }

       



    }
}

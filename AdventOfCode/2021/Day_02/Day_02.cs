﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace AdventOfCode2021
{
    public class Day_02 : BaseDay
    {
        /// <summary>
        /// Direction of the command
        /// </summary>
        enum Direction
        {
            Forward,
            Up,
            Down,
        }

        /// <summary>
        /// Represent a submarine command
        /// </summary>
        struct Command
        {
            public Direction direction;// Direction to move on
            public int value;// Value to move
        }

        private List<Command> commands;
        private int horizontal;
        private int depth;
        private int aims;
        private int plannedCourse;

        private string[] _input;
        string[] inputCommands;
        char[] delimiterChars = { ' '};

        public Day_02()
        {
            horizontal = 0;
            depth = 0;
            aims = 0;
            commands = new List<Command>();

            ReadInput();
        }

        public override ValueTask<string> Solve_1()
        {
            for (int i = 0; i < commands.Count; i++)
            {
                switch (commands[i].direction)
                {
                    case Direction.Forward:
                        horizontal += commands[i].value;
                        break;
                    case Direction.Up:
                        depth -= commands[i].value;
                        break;
                    case Direction.Down:
                        depth += commands[i].value;
                        break;
                    default:
                        break;
                }
            }

            plannedCourse = horizontal * depth;

            return new(plannedCourse.ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            horizontal = 0;
            depth = 0;
            aims = 0;

            for (int i = 0; i < commands.Count; i++)
            {
                switch (commands[i].direction)
                {
                    case Direction.Forward:
                        horizontal += commands[i].value;
                        depth += aims * commands[i].value;
                        break;
                    case Direction.Up:
                        aims -= commands[i].value;
                        break;
                    case Direction.Down:
                        aims += commands[i].value;
                        break;
                    default:
                        break;
                }
            }

            plannedCourse = horizontal * depth;

            return new(plannedCourse.ToString());
        }

        private void ReadInput()
        {
            _input = File.ReadAllLines(InputFilePath);

            for (int i = 0; i < _input.Length; i++)
            {
                inputCommands = _input[i].Split(delimiterChars);

                Command command;
                if (inputCommands.Length == 2)
                {
                    if (inputCommands[0] == "forward")
                    {
                        command.direction = Direction.Forward;
                        command.value = int.Parse(inputCommands[1]);
                        commands.Add(command);
                    }
                    else if (inputCommands[0] == "down")
                    {
                        command.direction = Direction.Down;
                        command.value = int.Parse(inputCommands[1]);
                        commands.Add(command);
                    }
                    else if (inputCommands[0] == "up")
                    {
                        command.direction = Direction.Up;
                        command.value = int.Parse(inputCommands[1]);
                        commands.Add(command);
                    }
                }
            }
        }
    }
}

﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace AdventOfCode2021
{
    public class Day_07 : BaseDay
    {
       
        string inputs;
        List<int> horizontalPositions;
        long fuel;

        public Day_07()
        {
            ReadInput();
        }

        public override ValueTask<string> Solve_1()
        {
            long costLeastPosition = Median();
            fuel = 0;

            for (int j = 0; j < horizontalPositions.Count; j++)
            {
                fuel += Math.Abs(horizontalPositions[j] - costLeastPosition);
            }

            return new(fuel.ToString());
        }

        //TO BE OPTIMISED
        public override ValueTask<string> Solve_2()
        {
            fuel = 0;
            long totalFuel = 0;

            Dictionary<int, List<long>> fuels = new Dictionary<int, List<long>>();
            List<long> fuelByPosition = new List<long>();

            for (int i = 0; i < horizontalPositions.Max(); i++)//Loop from zero to the max value
            {
                fuel = 0;
                totalFuel = 0;
                fuelByPosition = new List<long>();

                for (int k = 0; k < horizontalPositions.Count; k++)//Loop around all horizontal position in input file
                {
                    fuel = 0;
                    totalFuel = 0;
                    int diff = Math.Abs(horizontalPositions[k] - i);//Get the difference between the horizontal position and i index position

                    for (int j = 0; j < diff; j++)
                    {
                        fuel = fuel + 1;//Each turn cost 1 more
                        totalFuel += fuel;
                    }

                    fuelByPosition.Add(totalFuel);
                }

                fuels.Add(i, fuelByPosition);
            }

            var totalCost = fuels.Min(kvp => kvp.Value.Sum());

            return new(totalCost.ToString());
        }

        long Median()
        {
            int numberCount = horizontalPositions.Count();
            int halfIndex = horizontalPositions.Count() / 2;
            var sortedNumbers = horizontalPositions.OrderBy(n => n);
            long median;
            if ((numberCount % 2) == 0)
            {
                median = ((sortedNumbers.ElementAt(halfIndex) +
                    sortedNumbers.ElementAt((halfIndex - 1))) / 2);
            }
            else
            {
                median = sortedNumbers.ElementAt(halfIndex);
            }

            return median;
           
        }

        private void ReadInput()
        {
            inputs = File.ReadAllText(InputFilePath);
            horizontalPositions = inputs.Split(',').Select(Int32.Parse).ToList();
        }
    }
}

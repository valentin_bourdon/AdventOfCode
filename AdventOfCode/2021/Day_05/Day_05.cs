﻿using AoCHelper;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace AdventOfCode2021
{
    public class Day_05 : BaseDay
    {
        struct Vector
        {
            public List<Coordinate> coordinates;
        }

        struct Coordinate
        {
            public int x;
            public int y;
        }

        string[] inputs;
        List<Vector> vectors;
        int[,] diagram;

        public Day_05()
        {
            ReadInput();
        }

        public override ValueTask<string> Solve_1()
        {
            vectors = new List<Vector>();

            GetVectors(inputs,true,true,false);
            ConstructDiagram();

            return new(GetOverlapLines().ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            vectors = new List<Vector>();

            GetVectors(inputs, true, true, true);
            ConstructDiagram();

            return new(GetOverlapLines().ToString());
        }

        private void ConstructDiagram()
        {
            diagram = new int[1000, 1000];
            for (int i = 0; i < 1000; i++)
                for (int j = 0; j < 1000; j++)
                    diagram[i, j] = 0;

            for (int i = 0; i < vectors.Count; i++)
            {
                for (int j = 0; j < vectors[i].coordinates.Count; j++)
                {
                    int valueX = vectors[i].coordinates[j].x;
                    int valueY = vectors[i].coordinates[j].y;

                    diagram[valueX, valueY] += 1;
                }
            }
        }

        int GetOverlapLines()
        {
            int numberOfOverlapLines = 0;

            for (int i = 0; i < 1000; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    if (diagram[i, j] > 1)
                        numberOfOverlapLines++;
                }
            }

            return numberOfOverlapLines;
        }

        void Vertical(Vector line, Coordinate a, Coordinate b)
        {
            if(a.y == b.y)
            {
                if(a.x < b.x)
                {
                    Coordinate coordinate = new Coordinate();

                    for (int i = a.x + 1; i < b.x; i++)
                    {
                        coordinate.x = i;
                        coordinate.y = a.y;

                        line.coordinates.Add(coordinate);
                    }
                }
                else if (a.x > b.x)
                {
                    Coordinate coordinate = new Coordinate();

                    for (int i = b.x + 1; i < a.x; i++)
                    {
                        coordinate.x = i;
                        coordinate.y = a.y;

                        line.coordinates.Add(coordinate);
                    }
                }

                vectors.Add(line);
            }
        }

        void Horizontal(Vector line, Coordinate a, Coordinate b)
        {
            if(a.x == b.x)
            {
                if(a.y < b.y)
                {
                    Coordinate coordinate = new Coordinate();

                    for (int i = a.y+1; i < b.y; i++)
                    {
                        coordinate.x = a.x;
                        coordinate.y = i;

                        line.coordinates.Add(coordinate);
                    }
                }
                else if(a.y > b.y)
                {
                    Coordinate coordinate = new Coordinate();

                    for (int i = b.y + 1; i < a.y; i++)
                    {
                        coordinate.x = a.x;
                        coordinate.y = i;

                        line.coordinates.Add(coordinate);
                    }
                }

                vectors.Add(line);
            }
        }

        void Diagonal(Vector line, Coordinate a, Coordinate b)
        {
            if(a.x < b.x)
            {
                if(a.y < b.y)
                {
                    Coordinate coordinate = new Coordinate();
                    int k = 1;
                    for (int i = a.x + 1; i < b.x; i++)
                    {
                        coordinate.x = i;
                        coordinate.y = a.y + k;

                        k++;
                        line.coordinates.Add(coordinate);
                    }

                    vectors.Add(line);
                }
                else if(a.y > b.y)
                {
                    Coordinate coordinate = new Coordinate();
                    int k = 1;
                    for (int i = a.x + 1; i < b.x; i++)
                    {
                        coordinate.x = i;
                        coordinate.y = a.y-k;

                        k++;
                        line.coordinates.Add(coordinate);
                    }

                    vectors.Add(line);
                }
               
            }
            else if (a.x > b.x)
            {
                if (a.y < b.y)
                {
                    Coordinate coordinate = new Coordinate();
                    int k = 1;
                    for (int i = b.x + 1; i < a.x; i++)
                    {
                        coordinate.x = i;
                        coordinate.y = b.y-k;

                        k++;
                        line.coordinates.Add(coordinate);
                    }

                    vectors.Add(line);
                }
                else if(a.y > b.y){

                    Coordinate coordinate = new Coordinate();
                    int k = 1;
                    for (int i = b.x + 1; i < a.x; i++)
                    {
                        coordinate.x = i;
                        coordinate.y = b.y +k;

                        k++;
                        line.coordinates.Add(coordinate);
                    }

                    vectors.Add(line);
                }
            }
        }

        private void GetVectors(string[] _inputs, bool _horizontal, bool _vertical, bool _diagonal)
        {
            for (int i = 0; i < _inputs.Length; i++)
            {
                string[] values = _inputs[i].Split("->");

                string[] fistCoordinate = values[0].Split(',');
                string[] secondCoordinate = values[1].Split(',');

                Vector vector = new Vector();
                vector.coordinates = new List<Coordinate>();

                Coordinate coordinateA = new Coordinate();
                coordinateA.x = int.Parse(fistCoordinate[0]);
                coordinateA.y = int.Parse(fistCoordinate[1]);
                vector.coordinates.Add(coordinateA);

                Coordinate coordinateB = new Coordinate();
                coordinateB.x = int.Parse(secondCoordinate[0]);
                coordinateB.y = int.Parse(secondCoordinate[1]);
                vector.coordinates.Add(coordinateB);

                if(_horizontal)
                    Horizontal(vector, coordinateA, coordinateB);
                if (_vertical)
                    Vertical(vector, coordinateA, coordinateB);
                if (_diagonal)
                    Diagonal(vector, coordinateA, coordinateB);
            }
        }

        private void ReadInput()
        {
            inputs = File.ReadAllLines(InputFilePath);
        }
    }
}

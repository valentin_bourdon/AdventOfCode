﻿using AoCHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode2021
{
    public class Day_01 : BaseDay
    {
        private int[] _input;
        private int _numberOfIncreasedMeasurements;
        private List<int> _sumOfThreeMeasurements;
        private readonly int _windowLength = 3;

        public Day_01()
        {
            ReadInput();
        }

        public override ValueTask<string> Solve_1()
        {
            _numberOfIncreasedMeasurements = 0;

            for (int i = 1; i < _input.Length; i++)
            {
                if (_input[i] > _input[i - 1])
                    _numberOfIncreasedMeasurements++;
            }

            return new(_numberOfIncreasedMeasurements.ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            int _sum = 0;
            _sumOfThreeMeasurements = new List<int>();

            for (int i = 0; i < _input.Length; i ++)
            {
                _sum = 0;
                if (i + _windowLength <= _input.Length)
                {
                    for (int j = 0; j < _windowLength; j++)
                        _sum += _input[i + j];
                }
                else
                    break;
                   
                _sumOfThreeMeasurements.Add(_sum);
            }

            _numberOfIncreasedMeasurements = 0;
            for (int i = 1; i < _sumOfThreeMeasurements.Count; i++)
            {
                if (_sumOfThreeMeasurements[i] > _sumOfThreeMeasurements[i - 1])
                    _numberOfIncreasedMeasurements++;
            }

            return new(_numberOfIncreasedMeasurements.ToString());
        }

        private void ReadInput()
        {
            _input = File.ReadAllLines(InputFilePath).Select(int.Parse).ToArray();
        }
    }
}

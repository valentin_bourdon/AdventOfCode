﻿using AoCHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode2021
{
    public class Day_01 : BaseDay
    {
        private string[] _input;
        private List<int> _sumOfCaloriesPerElves;

        public Day_01()
        {
            ReadInput();
        }

        public override ValueTask<string> Solve_1()
        {         
            _sumOfCaloriesPerElves = new List<int>();

            int _sum = 0;
            for (int i = 0; i < _input.Length; i++)
            {
                if (!string.IsNullOrEmpty(_input[i]))//Is line is not empty
                {
                    int _nbCalories = int.Parse(_input[i]);
                    _sum += _nbCalories;                }
                else//Line is empty, we change of item's Calories. We can get the total of calories
                {
                    _sumOfCaloriesPerElves.Add(_sum);
                    _sum = 0;
                    continue;
                }

                if(i == _input.Length -1)//For last lines
                    _sumOfCaloriesPerElves.Add(_sum);
            }

            int _maxSumOfCalories = _sumOfCaloriesPerElves.Max();

            return new(_maxSumOfCalories.ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            _sumOfCaloriesPerElves.Sort();//Sort the list by order (min -> max)
            _sumOfCaloriesPerElves.Reverse(); //Reverse the list

            int _sumOfTopElves = _sumOfCaloriesPerElves.Take(3).Sum(); //Get the sum of three first items

            return new(_sumOfTopElves.ToString());
        }

        private void ReadInput()
        {  
            _input = File.ReadAllLines(InputFilePath).ToArray();
        }
    }
}

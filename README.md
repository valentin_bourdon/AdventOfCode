# Advent of Code

[Advent of Code](https://adventofcode.com/) is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like. People use them as a speed contest, interview prep, company training, university coursework, practice problems, or to challenge each other.

Puzzles are solved in C# using [AoCHelper](https://github.com/eduherminio/AoCHelper) library.

# AoCHelper

AoCHelper is a support library for solving Advent of Code puzzles, available for .NET and .NET Standard 2.x.
It provides a 'framework' so that you only have to worry about solving the problems, and measures the performance of your solutions.


- Create one class per day/problem, using one of the following approaches:
    - Name them ```DayXX``` or ```Day_XX``` and make them inherit ```BaseDay```.
    - Name them ```ProblemXX``` or ```Problem_XX``` and make them inherit ```BaseProblem```.
- Put your input files under ```Inputs/``` directory and follow ```XX.txt``` naming convention for day ```XX```. Make sure to copy those files to your output folder.
- Choose your solving strategy in your ```Main()``` method, adjusting it with your custom ```SolverConfiguration``` if needed:
    - ```Solver.SolveAll();```
    - ```Solver.SolveLast();```
    - ```Solver.SolveLast(new SolverConfiguration { ClearConsole = false });```
    - ```Solver.Solve<Day_05>();```
    - ```Solver.Solve(new List<uint>{ 5, 6 });```
    - ```Solver.Solve(new List<Type> { typeof(Day_05), typeof(Day_06) });```


## Puzzles List

- [2021](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2021/) 
- [2022](https://gitlab.com/valentin_bourdon/AdventOfCode/-/tree/master/AdventOfCode/2022/)

# Credit

Puzzles, Code, & Design: [Eric Wastl](https://twitter.com/ericwastl)

# Licence

See the [LICENSE](https://gitlab.com/valentin_bourdon/AdventOfCode/-/blob/master/LICENSE) file in the same directory.

